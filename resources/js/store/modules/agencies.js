// if we need to migrate other codes
// export const  state = require('./states/agencies').state
import axios from 'axios'
import * as types from '../mutation-types'

export const state = {
  all: []
}
// getters
export const getters = {
  all: state => state.all
}
// mutations
export const mutations = {
  [types.FETCH_AGENCIES] (state, { agencies }) {
    state.all = agencies
  },
  [types.FETCH_AGENCIES_SUCCESS] (state, { agencies }) {
    state.all = agencies
  },

  [types.FETCH_AGENCIES_FAILURE] (state) {
    state.all = []
  }
}
export const actions = {
  async fetchAgencies ({ commit }) {
    try {
      const { data } = await axios.get('/api/agencies')
      commit(types.FETCH_AGENCIES_SUCCESS, { agencies: data })
    } catch (e) {
      commit(types.FETCH_AGENCIES_FAILURE)
    }
  }
}
