// if we need to migrate other codes
// export const  state = require('./states/permissions').state
import axios from 'axios'
import * as types from '../mutation-types'

export const state = {
  all: []
}
// getters
export const getters = {
  all: state => state.all
}
// mutations
export const mutations = {
  [types.FETCH_PERMISSIONS] (state, { permissions }) {
    state.all = permissions
  },
  [types.FETCH_PERMISSIONS_SUCCESS] (state, { permissions }) {
    state.all = permissions
  },

  [types.FETCH_PERMISSIONS_FAILURE] (state) {
    state.all = []
  }
}
export const actions = {
  async fetchPermissions ({ commit }) {
    try {
      const { data } = await axios.get('/api/permissions')
      commit(types.FETCH_PERMISSIONS_SUCCESS, { permissions: data })
    } catch (e) {
      commit(types.FETCH_PERMISSIONS_FAILURE)
    }
  }
}
