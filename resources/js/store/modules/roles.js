// if we need to migrate other codes
// export const  state = require('./states/roles').state
import axios from 'axios'
import * as types from '../mutation-types'
// states
export const state = {
  all: {
    rows: {
      data: []
    },
    pagination: {}
  },
  role: {}
}
// getters
export const getters = {
  all: state => state.all,
  rows: state => state.all.rows.data,
  pagination: state => state.all.pagination,
  role: state => state.role
}
// mutations
export const mutations = {
  [types.FETCH_ROLES] (state, { roles }) {
    state.all = roles
  },
  [types.FETCH_ROLES_SUCCESS] (state, { roles }) {
    state.all = roles
  },

  [types.FETCH_ROLES_FAILURE] (state) {
    state.all = {}
  },

  [types.FETCH_ROLE] (state, { role }) {
    state.role = role
  },
  [types.FETCH_ROLE_SUCCESS] (state, { role }) {
    state.role = role
  },

  [types.FETCH_ROLE_FAILURE] (state) {
    state.role = {}
  }
}
// actions
export const actions = {
  async fetchRoles ({ commit }) {
    try {
      const { data } = await axios.get('/api/roles')
      commit(types.FETCH_ROLES_SUCCESS, { roles: data })
    } catch (e) {
      commit(types.FETCH_ROLES_FAILURE)
    }
  },
  async fetchRole ({ commit }, payload) {
    try {
      const { data } = await axios.get('/api/roles/' + payload)
      commit(types.FETCH_ROLE_SUCCESS, { role: data })
    } catch (e) {
      commit(types.FETCH_ROLE_FAILURE)
    }
  }
}
