// if we need to migrate other codes
// export const  state = require('./states/users').state
import axios from 'axios'
import * as types from '../mutation-types'

export const state = {
  all: {
    rows: {
      data: []
    },
    pagination: {}
  },
  user: {}
}
// getters
export const getters = {
  all: state => state.all,
  rows: state => state.all.rows.data,
  pagination: state => state.all.pagination,
  user: state => state.user
}
// mutations
export const mutations = {
  [types.FETCH_USERS] (state, { users }) {
    state.all = users
  },
  [types.FETCH_USERS_SUCCESS] (state, { users }) {
    state.all = users
  },

  [types.FETCH_USERS_FAILURE] (state) {
    state.all = []
  },
  [types.FETCH_USER] (state, { user }) {
    state.user = user
  },
  [types.FETCH_USER_SUCCESS] (state, { user }) {
    state.user = user
  },

  [types.FETCH_USER_FAILURE] (state) {
    state.user = {}
  }
}
export const actions = {
  async fetchUsers ({ commit }) {
    try {
      const { data } = await axios.get('/api/users')
      commit(types.FETCH_USERS_SUCCESS, { users: data })
    } catch (e) {
      commit(types.FETCH_USERS_FAILURE)
    }
  },
  async fetchUser ({ commit }, payload) {
    try {
      const { data } = await axios.get('/api/users/' + payload)
      commit(types.FETCH_USER_SUCCESS, { user: data })
    } catch (e) {
      commit(types.FETCH_USER_FAILURE)
    }
  }
}
