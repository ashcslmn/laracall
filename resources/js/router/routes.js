const Welcome = () => import('~/pages/welcome').then(m => m.default || m)
const Login = () => import('~/pages/auth/login').then(m => m.default || m)
const Register = () => import('~/pages/auth/register').then(m => m.default || m)
const PasswordEmail = () => import('~/pages/auth/password/email').then(m => m.default || m)
const PasswordReset = () => import('~/pages/auth/password/reset').then(m => m.default || m)
const NotFound = () => import('~/pages/errors/404').then(m => m.default || m)

const Home = () => import('~/pages/home').then(m => m.default || m)
const Settings = () => import('~/pages/settings/index').then(m => m.default || m)
const SettingsProfile = () => import('~/pages/settings/profile').then(m => m.default || m)
const SettingsPassword = () => import('~/pages/settings/password').then(m => m.default || m)

const Agencies = () => import('~/pages/agencies').then(m => m.default || m)
const Engines = () => import('~/pages/engines').then(m => m.default || m)

const Users = () => import('~/pages/users').then(m => m.default || m)
const UserShow = () => import('~/pages/users/show').then(m => m.default || m)
const UserNew = () => import('~/pages/users/form').then(m => m.default || m)

const Roles = () => import('~/pages/roles').then(m => m.default || m)
const RolesChild = () => import('~/pages/roles/child').then(m => m.default || m)
const RoleShow = () => import('~/pages/roles/show').then(m => m.default || m)
const RoleNew = () => import('~/pages/roles/form').then(m => m.default || m)
const Permissions = () => import('~/pages/permissions').then(m => m.default || m)

export default [
  { path: '/', name: 'welcome', component: Welcome },

  { path: '/login', name: 'login', component: Login },
  { path: '/register', name: 'register', component: Register },
  { path: '/password/reset', name: 'password.request', component: PasswordEmail },
  { path: '/password/reset/:token', name: 'password.reset', component: PasswordReset },

  { path: '/home', name: 'home', component: Home },
  { path: '/agencies', name: 'agencies', component: Agencies },
  { path: '/engines', name: 'engines', component: Engines },
  { path: '/users', name: 'users', component: Users },
  { path: '/roles',
    name: 'users.child',
    component: RolesChild,
    children: [
      { path: '/users/new', name: 'users.new', component: UserNew, props: true },
      { path: '/users/:id', name: 'users.show', component: UserShow, props: true }
    ]
  },
  { path: '/roles', name: 'roles', component: Roles },
  { path: '/roles',
    name: 'roles.child',
    component: RolesChild,
    children: [
      { path: '/roles/new', name: 'roles.new', component: RoleNew, props: true },
      { path: '/roles/:id', name: 'roles.show', component: RoleShow, props: true }
    ]
  },
  { path: '/permissions', name: 'permissions', component: Permissions },
  { path: '/settings',
    component: Settings,
    children: [
      { path: '', redirect: { name: 'settings.profile' } },
      { path: 'profile', name: 'settings.profile', component: SettingsProfile },
      { path: 'password', name: 'settings.password', component: SettingsPassword }
    ] },

  { path: '*', component: NotFound }
]
