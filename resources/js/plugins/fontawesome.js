import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// import { } from '@fortawesome/free-regular-svg-icons'

import {
  faUser, faLock, faSignOutAlt, faCog, faHome, faSearch, faPhone, faSitemap, faProjectDiagram, faCube, faFileContract, faCogs, faUsers, faEye, faTrash, faPencilAlt
} from '@fortawesome/free-solid-svg-icons'

import {
  faGithub
} from '@fortawesome/free-brands-svg-icons'

library.add(
  faUser, faLock, faSignOutAlt, faCog, faHome, faSearch, faPhone, faSitemap
  , faGithub, faProjectDiagram, faCube, faFileContract, faCogs, faUsers, faEye, faPencilAlt, faTrash
)

Vue.component('fa', FontAwesomeIcon)
