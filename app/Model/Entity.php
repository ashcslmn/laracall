<?php
namespace App\Model;

use Log;

class Entity {
    
    /**
     * generate id for account
     * @param null
     * @return string id
     */
    public static function generateIdforAccount () {
     
        $randLetters = md5(uniqid(rand(), true));
        $count = Account::count() + 1; 
        $id = sprintf("AC%s%07d", strtoupper(substr($randLetters, -3)), $count);
        
        return $id;
    }

    public static function generateIdforAgency () {
     
        $randLetters = md5(uniqid(rand(), true));
        $count = Agency::count() + 1; 
        $id = sprintf("AG%s%07d", strtoupper(substr($randLetters, -3)), $count);
        
        return $id;
    }


}
