<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Engine extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];
}
