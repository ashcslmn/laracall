<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'description'
    ];

    /**
     * @param array $attributes
     * @return User
     */
    public static function create(array $attributes = [])
    {
        $attributes = array_merge([ 'id' => Entity::generateIdforAgency() ], $attributes );
        return static::query()->create($attributes);
    }

    public function users () {
        return $this->belongsToMany(User::class);
    }

}
