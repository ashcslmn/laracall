<?php

namespace App\Http\Controllers;

use App\Model\Agency;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AgenciesController extends Controller
{
    /**
     * Get the users.
     * @return array User
     */
    public function index()
    {
        $agencies = Agency::all();
        return $agencies;
    }

    public function create(Request $request) {

        $user = $request->user();

        $this->validate($request, [
            'name' => 'required',
        ]);

        return Agency::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
        ]);
    }
}
