<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = DB::table("roles")->get();
        foreach($roles as $role) {
            DB::table("roles")->where('id',$role->id)->delete();
        }
        $permissions = [
            'role-view',
            'role-create',
            'role-edit',
            'role-delete',
            'agency-list',
            'agency-create',
            'agency-edit',
            'agency-delete',
            'user-view',
            'user-create',
            'user-edit',
            'user-delete'
         ];
        $roles = DB::table("permissions")->whereIn('name', $permissions)->delete();
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
      
        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
       
        $role = Role::create(['name' => 'super-admin']);
        $role->givePermissionTo(Permission::all());

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo(['user-view']);

        $role = Role::create(['name' => 'manager']);
        $role->givePermissionTo(['user-view']);

        $role = Role::create(['name' => 'reporter']);
        $role->givePermissionTo(['user-view']);

        $role = Role::create(['name' => 'informer']); // notifications
        $role->givePermissionTo(['user-view']);
    }
}
